# Instance the local provider
terraform {
  required_version = ">= 1.1.7"
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.6.14"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_network" "t10k3s" {
  name = "t10k3s-network"
  mode = "nat"

  addresses = ["10.1.10.0/28"]
  domain    = "k3s.local"

  autostart = true

  dhcp {
    enabled = false
  }

  dns {
    enabled = true

    local_only = false
    forwarders { address = "127.0.0.53" }

    hosts  {
        hostname = "t1master1a"
        ip = "10.1.10.10"
      }
    hosts {
        hostname = "t1compute1a"
        ip = "10.1.10.11"
      }
    hosts {
        hostname = "t1compute1b"
        ip = "10.1.10.12"
      }
    hosts {
        hostname = "t1compute1c"
        ip = "10.1.10.13"
      }
  }  
}
