# Instance the local provider
terraform {
  required_version = ">= 1.0.7"
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.6.11"
    }
  }
}

provider "libvirt" {
  alias = "p1kvm-01"
  uri = "qemu+ssh://administrator@192.168.1.22/system" 

}

provider "libvirt" {
  alias = "p1kvm-02"
  uri = "qemu+ssh://aed@192.168.1.200/system"
}
